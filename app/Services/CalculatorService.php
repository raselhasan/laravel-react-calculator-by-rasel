<?php 
namespace App\Services;

class CalculatorService{

    public function addition( float $number1, float $number2)
    {   
        $result = number_format($number1 + $number2, 2);
        $result = 'Addition of '.$number1.' and '. $number2 .' is: '. $result;
        return response()->json(['success'=>true, 'data'=> $result]);
    }
    public function subtraction( float $number1, float $number2)
    {
        $result = number_format($number1 - $number2, 2);
        $result = 'Subtraction of '.$number1.' and '. $number2 .' is: '. $result;
        return response()->json(['success'=>true, 'data'=> $result]);
    }
    public function multiplication( float $number1, float $number2)
    {
        $result = number_format($number1 * $number2, 2);
        $result = 'Multiplication of '.$number1.' and '. $number2 .' is: '. $result;
        return response()->json(['success'=>true, 'data'=> $result]);
    }
    public function division( float $number1, float $number2)
    {
        $result = number_format($number1 / $number2, 2);
        $result = 'Division of '.$number1.' and '. $number2 .' is: '. $result;
        return response()->json(['success'=>true, 'data'=> $result]);
    }
    public function generateResult( $request )
    {
        $number1    = $request->number1;
        $number2    = $request->number2;
        $operator   = $request->operator;

        switch($operator){
            case "+":
                return $this->addition($number1, $number2);
                break;
            case "-":
                return $this->subtraction($number1, $number2);
                break;
            case "*":
                return $this->multiplication($number1, $number2);
                break;
            case "/":
                return $this->division($number1, $number2);
                break;
            default:
                return response()->json(['success'=>false, 'message'=>'Invalid Operation'], 422);
                break;                
        }
    }
}



