<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculatorRequest;
use App\Services\CalculatorService;
class CalculatorController extends Controller
{
    public function calculate( CalculatorRequest $request, CalculatorService $calculatorService)
    {
        return $calculatorService->generateResult($request);
    }

    
}
