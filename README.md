## About Laravel React Calculator

Laravel react calculator provide calculation service include Addition, Subtraction, 
Multiplication and Division withing 2 numbers. Here used laravel as a backend and react as a frontend.

## Prerequisite

- PHP >= 7.4
- Composer >= 2.0
- Node >= 14.0

## Installation guide 

- go to project root directory
- create a new file inside root directory called (.env )
- copy .env.example content to .env file and save it
- composer update ( You can use also composer install )
- run this command - php artisan key:generate
- npm install
- npm run dev (If node version is different error can appare run this command 2 times)
- php artisan serve
- type on browser http:://localhost:8000

