import { configureStore } from '@reduxjs/toolkit'
import { combineReducers } from 'redux'
import calculatorSlice from './features/calculator/calculatorSlice';
const reducer = combineReducers({
  calculator: calculatorSlice,
})
const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false
  })
})
export default store;