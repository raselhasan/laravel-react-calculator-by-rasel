import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { API_URI } from "../../constants";

export const GetCalculation = createAsyncThunk(
    "calculator/getCalculation", async ( data,{ rejectWithValue } ) => {
        try {
            return await axios.post(`${API_URI}/calculate`, {
                ...data
            })
        } catch (error) {
            return rejectWithValue(error.response.data)
        }
    }
);