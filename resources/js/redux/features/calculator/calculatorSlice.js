import { createSlice } from '@reduxjs/toolkit';
export const initialState = {
    result:'',
    loading:false,
    errors:{},
};

const calculatorSlice = createSlice({
    name: 'calculator',
    initialState,
    reducers: {
        resetError: (state, action) => {
            state.errors = {};
        },
    },
    extraReducers: {
        ['calculator/getCalculation/pending']: (state) => {
            state.loading = true
        },

        ['calculator/getCalculation/fulfilled']: (state, action) => {
            state.result = action.payload.data.data;
            state.loading = false
        },
        ['calculator/getCalculation/rejected']: (state, action) => {
            state.result = ''
            state.loading = false
            state.errors = action.payload
            console.log("Error Action",action.payload)
        },
    },
});
export const { resetError } = calculatorSlice.actions;
export default calculatorSlice.reducer;