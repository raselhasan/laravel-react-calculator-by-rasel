import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch, useSelector } from 'react-redux';
import { GetCalculation } from '../../redux/features/calculator/services';
import { _calculatorVSchema } from './Validation';
 
const Calculator = () => {
    const dispatch = useDispatch();
    const result = useSelector(state => state.calculator.result)
    const loading = useSelector(state => state.calculator.loading)
    const serverErrors = useSelector(state => state.calculator.errors)
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(_calculatorVSchema())
    });
    const onSubmit = async (data) => {
        dispatch(GetCalculation(data))
    };
    return (
        <div className="container mt-4">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Enter your number and calculate it.</div>
                        <div className="card-body">
                            {result && <div className="row">
                                <div className="col-md-12">
                                    <div className="result_item">
                                        {result}
                                    </div>
                                </div>
                            </div>}
                            {serverErrors?.message && 
                            <div className="row">
                                <div className="col-md-12">
                                    <div class="alert alert-danger" role="alert">
                                        {serverErrors?.message}
                                    </div>
                                </div>
                            </div>}
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input 
                                                type="number" 
                                                className={`form-control ${errors.number1 ? 'inputError' : ''}`}
                                                placeholder="Enter First Number" 
                                                {...register('number1')} 
                                            />
                                            <small className="form-text text-danger">{errors.number1?.message}</small>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input 
                                                type="number" 
                                                className={`form-control ${errors.number2 ? 'inputError' : ''}`}
                                                placeholder="Enter Second Number"
                                                {...register('number2')}  
                                            />
                                            <small className="form-text text-danger">{errors.number2?.message}</small>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <select className={`form-control ${errors.operator ? 'inputError' : ''}`} {...register('operator')} >
                                                <option value="">--- Select Options ---</option>
                                                <option value="+"> &#128125; Addition  </option>
                                                <option value="-"> &#128128; Subtraction </option>
                                                <option value="*"> &#128123; Multiplication </option>
                                                <option value="/"> &#128561; Division </option>
                                            </select>
                                            <small className="form-text text-danger">{errors.operator?.message}  </small>
                                        </div>
                                    </div>
                                    <div className="col-md-6 form-group">
                                        <button 
                                            type="submit" 
                                            className={ loading ? 'btn btn-warning' : 'btn btn-primary'}
                                            disabled= { loading ? true : false}
                                            >
                                                { loading && <i class="fa fa-circle-o-notch fa-spin"></i> }
                                                { loading ? ' Loading' : 'Calculate'}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Calculator;
