import * as Yup from 'yup';

export const _calculatorVSchema = () =>{
    return Yup.object().shape({
        number1: Yup.number()
          .required('First number is required')
          .min(1, 'Number must be at least 1 characters'),
        number2: Yup.number()
          .required('Second number is required')
          .min(1, 'Second must be at least 1 characters'),
        operator: Yup.string()
          .required('Please select valid option')
    });
}