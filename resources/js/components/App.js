import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './Calculator/Calculator';
import Nav from './Nav/Nav'
function App() {
    return (
        <>
            <Nav/>
            <Calculator />
        </>
    );
}

export default App;
